#!/usr/bin/env bash

${HOME}/repos/cppinclude/cppinclude --report_limit=3 --ignore_files="SDL2|catch|tiny"
