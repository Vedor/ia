// =============================================================================
// Copyright 2011-2022 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef ITEM_CURSE_IDS_HPP
#define ITEM_CURSE_IDS_HPP

namespace item_curse
{
enum class Id
{
        hit_chance_penalty,
        increased_shock,
        heavy,
        shriek,
        teleport,
        summon,
        fire,
        cannot_read,
        light_sensitive,

        END
};

}  // namespace item_curse

#endif  // ITEM_CURSE_IDS_HPP
